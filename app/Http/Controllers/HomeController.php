<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\User;
use Auth;
// use Session;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $status=auth::user()->status;
        if($status==$status)
        {
            auth()->user()->assignRole('admin');
            $user=auth::user()->name;
            if($user==$user)
            {
                // dd($user);
                $iduser = User::where('name',$user)->pluck('id_emp');
                    // dd($iduser);
                    auth()->user()->assignRole('admin');
                if($iduser==$iduser)
                {
                    $profile = Employee::where('id_emp',$iduser)->get()->toArray();
                    // dd($profile);
                    return view('layouts.admin_detail',compact('profile'));
                }
            }
        }
    }
    
}
