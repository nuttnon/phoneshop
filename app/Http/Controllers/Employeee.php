<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
// use Session;
class Employeee extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $emp = Employee::all()->toArray();
        return view('layouts.admin_Manageemployees',compact('emp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'id_emp' => 'required',
            'name' => 'required',
            'lastname' => 'required',
            'address' => 'required',
            'tel' => 'required',
            'email' => 'required',
            'position' => 'required',
        ]);
        $emp = new Employee([
            'id_emp' =>$request->get('id_emp'),
            'name' =>$request->get('name'),
            'lastname' =>$request->get('lastname'),
            'address' =>$request->get('address'),
            'email' =>$request->get('email'),
            'tel' =>$request->get('tel')]);
            
        $emp->save();
        return redirect()->action('Employeee@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $editpro = Employee::all()->find($id);
        // dd($editpro);
        return view('layouts.admin_editperson',compact('editpro','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'lastname' => 'required',
            'address' => 'required',
            'tel' => 'required',
        ]);

        // $formdata=array(
        //     'name'=>$request->name,
        //     'lastname'=>$request->lastname,
        //     'address'=>$request->address,
        //     'tel'=>$request->tel,
        // );
        $updatepro = Employee::find($id);
        $updatepro->name = $request->get('name');
        $updatepro->lastname = $request->get('lastname');
        $updatepro->address = $request->get('address');
        $updatepro->tel = $request->get('tel');
        $updatepro->save();
        return redirect()->action('HomeController@index')->with('success','อัพเดทเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // dd($id);
        $empdel = Employee::where('id_emp',$id);
        $empdel->delete();
        return redirect()->action('Employeee@index');

    }
}
