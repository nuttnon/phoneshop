<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function() {
    return view('home');
});

// Route::get('/login',function() {
//     return auth('login');
// });

Route::get('/service',function() {
    return view('service');
});

Route::get('/contact',function() {
    return view('contact');
});

// Route::get('profile',function() {
//     return view('layouts.admin_detail');
// });
Route::resource('Mngemp','Employeee');

// Route::resource('editprofile','EditProfile');
Route::resource('Mnglog','UsermngController');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
// Route::get('login', 'HomeController@log')->name('login');

