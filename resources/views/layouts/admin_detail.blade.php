@extends('layouts.admin_master')
@section('title','admin_detail')
@section('page_header')
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">ประวัติส่วนตัว</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">ข้อมูลส่วนตัว</a></li>
              <li class="breadcrumb-item active">ประวัติส่วนตัว</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
    <!-- /.content-header -->
    @stop
@section('content')
      <div class="row">
          <div class="col-lg-3">
                <h5 class="card-title">
                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUPDw8VFRUVFRUVFRUVFRUVFRUVFRUXFhUVFRUYHSggGBolHRUXITEhJSktLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDysZFRkrKy03Ny0rLSstNzcrKzctKysrKysrKy0rNysrKysrNy0rKysrKysrLSsrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQIDBAUH/8QAMBABAQACAAIJBAAGAwEAAAAAAAECEQSRAxQhMUFRYXGxMoHB8BIiQlKh0QWC4SP/xAAWAQEBAQAAAAAAAAAAAAAAAAAAAQL/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD9WakJFaQWQkaBFGtAml0q6BNLpTQJo00AhpdAJo00gImmjQM6TTQDGk03pNAwmm7E0DCNpYDCNJQZGtAKshGoAshI1ATTUhpYBBVgIujS6BNCqCGlATRpQERoBnSNaTQM6GtIDOmbG6mgYsRuxmgzpltmggoCxYNQCNQkUBRQFIoAKCGWUk3bqernxHTzCedvdPzfR8/pM7ld5XfxPaA9mfGYzulv+JzrneNv9s515QHpnG5f2znXTDjZ4yz/AC8QD6uGcym5ZVfKxysu5dV7+G4j+Lsvf8+wO2hUBEaQGUaQGdM1upQYrLdjIMqoCxpI1AWKkaAiigKADPSZzGXK+DTyf8hn3Y/e/bu/fQHkyyuV3e+/ukAQAAAAWXXbEAfT6HpP4sd8/d0eLgM/5rj5z/Me0URQGajSUGUrVQGKzW6zQZF0A1GkiwFiwiwFCKAAA+dxl/8ApfSSfn8vovm8V9eX2+II5AAAAAAAA6cNdZ4+/wA9j6b5fQ/VPefL6oqAAgqAlZaqUGWa1UoMo0AsaZjQLFIoCooAAD5/GzWd9ZL+Pw+i8X/IY/Tfefn/AGDyACAAAAAAOvCzeePv8dr6Tw8Bj/Nb5T5/a94qACCKgqJWkoM1lqpQZ0KAsWJFBpYkWApAAABXHi8N4X07eX7XYB8cb6bD+HKzl7eDAgAAAAC4422SePYD38DhrDfnd/bw/L0JJrsndFFQAQAFSpVqUEZrTNBBQCLEig1FiRYCgQBUAUAHl47o9yZTw7/b9+XhfXs32V8rpMP4crj5fHgDIAgAA9fAdH23L7T8/vq8sm7qePY+r0eExkxngK0ACACFAFRKtSgjNVKCAAsWMxqA1FZig0IoAAKIoD5/HfX/ANZ816+n6fHDv7/CeP8A4+bnlcrcr30EAEAAdeF+ufvg+m+PK+j0HEzLsvZfn2FdwQAABFASpVZoIlVKCCbAWLGY1AaixloFio8vTcZ4Yc/D7eYPW558RhO/KfPw+bnlcvqtvv3ckEe3PjZ/Tjb79jh0nFZ5eOvb/biAAAAAAAAA69HxGePdd+l7XfDjp/Vjy7XjAfSw4nC/1c+z5dXyFxtndbPbsFfWR4+h4u92XPy93r2BUVkCs1azQBABqMRqA2sZUHLjOk1jqePZ9vH99Xhejjr24z0vzHnAAEAAAAAAAAAAAAAAHs4LpOy4+Xd7PG78Hf5/tRXtSiUESlS0EDaANSsRqA3FZiwHl4z6p7flweriOiyyss13ebn1bP05g4jt1XP05nVc/TmDiO/Vc/KczqufpzBwHfqmflOZ1TPynMHAd+qZ+U5nVM/KcwcB36pn5TmdUz8pzBwHfqmflOZ1TPynMHAd+q5+U5nVc/KcwcB26rn6czqufpzBxduF+uff4OrZ+nNvoOhyxy3dePiD01KVKCVKVKAIAkaYalBqNRiLKDcVmLsGl2yoNDK7BoZ2uwUTZsFE2bBRNoCiIC2obSgbQQC1mlqAVKVkF2IAkVlZQblVhdg3tZWQG9rtjayg3s2yuwaENg0MgNbGdmwUTYC7TabTYKlpalAtS1CgJTbIKgzaAIApABogAsa/fgAFgAtUAIoAAAAAIACACVKACADNABKyAMgA/9k=" width="150" height="160"> </h5>

                <p class="card-text">
                  <h2>{{ Auth::user()->lastname }}</h2>
                </p>
                @foreach($profile as $prorows)
                <a href="{{ action('Employeee@edit',$prorows['id_emp'])}}" class="btn btn-info btn-lg btn-block">แก้ไขข้อมูลส่วนตัว</a>
                <a href="#" class="btn btn-primary btn-lg btn-block">เปลี่ยนรหัสผ่าน</a>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-9">
            <div class="card">
              <div class="card-header bg-primary">
                <h3 class="m-0 ">ข้อมูลพนักงาน</h3>
              </div>
              @if(Session::has('success'))
              <div class="alert alert-success alupdate">
              <p>{{ Session::get('success')}}<p>
              </div>
              @endif
              <div class="card-body">
                <p class="card-text">
                <h6>รหัสพนักงาน : {{ $prorows['id_emp'] }}</h6><br>
                <h6>ชื่อ : {{ $prorows['name'] }}</h6><br>
                <h6>นามสกุล : {{ $prorows['lastname'] }}</h6><br>
                <h6>Email : {{ $prorows['email'] }}</h6><br>
                <h6>เบอร์โทรศัพท์ : {{ $prorows['tel'] }}</h6><br>
                @endforeach
                </p>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        @stop
@section('scripts')
   <script>
   $(document).ready(function () {
      $('.alupdate').fadeOut(2500);
   });
        </script>
        @stop