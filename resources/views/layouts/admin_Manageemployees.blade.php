@extends('layouts.admin_master')
@section('title','admin_changepass')
@section('page_header')
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">จัดการพนักงาน</h1>
          </div>
        </div>
    @stop
@section('content')
      <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header bg-primary">
                <h3 class="m-0 ">จัดการพนักงาน</h3>
              </div>
              <div class="card-body">
              <!-- <table class="table table-borderless"> -->
              <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">รหัสพนักงาน</th>
                  <!-- <th scope="col">ไอดี</th> -->
                  <th scope="col">ชื่อ</th>
                  <th scope="col">นามสกุล</th>
                  <th scope="col">ที่อยู่</th>
                  <th scope="col">เบอร์โทร</th>
                  <th scope="col">อีเมลล์</th>
                  <th scope="col">ตำแหน่ง</th>
                  <th scope="col">แก้ไข</th>
                  <th scope="col">ลบ</th>
                </tr>
              </thead>
              <tbody>
              @foreach($emp as $row)
                <tr>
                  <td>{{ $row['id_emp'] }}</td>
                  <!-- <td>{{ $row['id'] }}</td> -->
                  <td class="name">{{ $row['name'] }}</td>
                  <td class="lastname">{{ $row['lastname'] }}</td>
                  <td>{{ $row['address'] }}</td>
                  <td class="tel">{{ $row['person_id'] }}</td>
                  <td class="email">{{ $row['email'] }}</td>
                  <td>{{ $row['position'] }}</td>
                  <td>
                  <button type="button " class="btn btn-success" data-toggle="modal" data-target="#addEMP">
                  เพิ่มผู้ใช้งาน
                </button>
                  <td><button type="button" class="btn btn-success">แก้ไข</button></td>
                  <td>
                  <form method="post" class="delete_form" onclick="return confirm('You want to delete?')" action="{{action('Employeee@destroy',$row['id_emp'])}}">
                  @method('DELETE')
                  @csrf
                  <button  type="submit" class="btn btn-warning">ลบ</button>
                  </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>

        <div class="modal fade align-self-end" id="addEMP" tabindex="-1" role="dialog" aria-labelledby="addEMP" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">{{ __('Register') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <form method="POST" action="{{ action('Auth\RegisterController@register') }}">
                            @csrf
                            <p>
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $row['name'] }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('lastName') }}</label>
                                <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ $row['lastname'] }}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $row['email'] }}" required autocomplete="username">

                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                <input id="password" type="text" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ $row['person_id'] }}" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="text" class="form-control" name="password_confirmation" value="{{ $row['person_id'] }}" required autocomplete="new-password">
                            <div class="form-group">
                            <label for="status">ตำแหน่ง : </label>
                                <select name="status" id="status">
                                  <option value="1">admin</option>
                                  <option value="พนักงานทั่วไป">พนักงานทั่วไป</option>
                                  <option value="พนักงานบัญชี">พนักงานบัญชี</option>
                                  <option value="พนักงานคลัง">พนักงานคลัง</option>
                                  <option value="ช่างซ่อม">ช่างซ่อม</option>
                                </select>
                              </div>
                                <input type="hidden" name="id_emp" value="{{ $row['id_emp'] }}">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button></p>
                                </div>
                               </div>
                               </form>
                               </div>
                               </div>

        @stop