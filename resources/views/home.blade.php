@extends('master')
@section('title','HomePage')
@section('banner')
             <!-- banner -->
             <div class="container">
               <div class="style-banner text-center">
               <br><br><br><br><br><br>
                  <h4 class="mb-2">Welcome Mc Mobile Corner</h4>
               </div>
               <div class="two-demo-button d-flex justify-content-center mt-lg-5 mt-md-4 mt-sm-4 mt-3">
                  <div class="read-buttn ">
                  </div>
                  <div class="view-buttn">
                  </div>
               </div>
            </div>
         </div>

      <!-- //banner -->
      @stop
@section('content')
      <!-- about -->
      <section class="about py-lg-4 py-md-3 py-sm-3 py-3" id="about">
         <div class="container py-lg-5 py-md-4 py-sm-4 py-3">
            <h3 class="title text-center mb-md-4 mb-sm-3 mb-3 mb-2">MC Mobile Corner</h3>
            <div class="title-wls-text text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
               <p>รับซ่อมมือถือและอุปกรณ์สื่อสารทุกชนิด <br> iphone ipad samsung sony oppo vivo 085-6140001
               </p>
            </div>
            <div class="row pt-lg-5 pt-md-4 pt-3">
               <div class="col-lg-3 col-md-6 text-center col-sm-6 corpo-about ">
                  <div class="position-relative about-top-grid">
                     <div class="about-img-top">
                        <img src="images/a1.jpg" alt="" class="img-fluid">
                     </div>
                     <div class="about-wthree-about text-center mt-lg-4 mt-3">
                        <h5>เปิดแล้วนะครับร้าน Mc mobile corner</h5>
                        <p class="mt-2">ซ่อมมือถือด่วนเชียงใหม่ 085-641-0001</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-md-6 col-sm-6 text-center corpo-about ">
                  <div class="position-relative about-top-grid">
                     <div class="about-img-top">
                        <img src="images/a2.jpg" alt="" class="img-fluid">
                     </div>
                     <div class="about-wthree-about text-center mt-lg-4 mt-3">
                        <h5>เราซ่อมจริงๆจัง</h5>
                        <p class="mt-2">ไม่ว่าจะพังแบบไหนเราามารถซ่อมได้</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-md-6 col-sm-6 text-center corpo-about ">
                  <div class="position-relative about-top-grid">
                     <div class="about-img-top">
                        <img src="images/a3.jpg" alt="" class="img-fluid">
                     </div>
                     <div class="about-wthree-about text-center mt-lg-4 mt-3">
                        <h5>ซ่อมมือถือด่วน คิดถึงเรานะ
                        </h5>
                        <p class="mt-2">ศูนย์ซ่อมมือถือ โมบายคอร์นเนอร์ @เชียงใหม่</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-md-6 col-sm-6 text-center corpo-about ">
                  <div class="position-relative about-top-grid">
                     <div class="about-img-top">
                        <img src="images/a4.jpg" alt="" class="img-fluid">
                     </div>
                     <div class="about-wthree-about text-center mt-lg-4 mt-3">
                        <h5>อาการจอแตกเราก็เปลี่ยนให้ได้</h5>
                        <p class="mt-2">สามารถส่งซ่อมได้ ทั้งที่อะไหล่ชำรุดเรายินดีรับบริการ</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      @stop