
   <!DOCTYPE html>
<html lang="en-US">
   <head>
      <title>@yield('title')</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="Economy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
         Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
      <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script>
      <!--//meta tags ends here-->
      <!--booststrap-->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <!-- //font-awesome icons -->
      <!--stylesheets-->
      <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <link href="//fonts.googleapis.com/css?family=Titillium+Web:400,600,700" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet">
   </head>
   <body>
       <header>
       <div class="banner-inner" id="home">
         <!-- header -->
         <div class="headder-top">
            <div class="container-fluid">
               <!-- nav -->
               <nav>
                  <div id="logo">
                     <h1><a href="/"><img src="../images/logo.png" width="100" height="60"><span aria-hidden="true"></span>McMobileCorner</a></h1>
                  </div>
                  <label for="drop" class="toggle">Menu</label>
                  <input type="checkbox" id="drop">
                  <ul class="menu mt-2">
                     <li><a class="active" href="/">Home</a></li>
                     <li><a href="service">Cheaking status</a></li>
                     <li class="mx-lg-3 mx-md-2 my-md-0 my-1">
                     <li><a href="contact">Contact Us</a></li>
                     <li><a href="login" class="drop-text">login</a></li>
                  </ul>
               </nav>
               <!-- //nav -->
            </div>
         </div>
         <div class="main-banner" id="box">
         @yield('banner')
        </div>  
         <!-- //header -->
         
        </header>
        <div class="container" id="boxcontent">
        @yield('content')
        </div>
    </body>
    
      <!-- footer -->
      <footer class="py-lg-4 py-md-3 py-sm-3 py-3">
         <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
            <div class="row ">
               <div class="footer-info-bottom col-lg-4 col-md-3">
                  <h4 class="pb-lg-4 pb-md-3 pb-3">Address</h4>
                  <div class="bottom-para ">
                     <div class="footer-office-hour">
                        <ul>
                           <li class="mb-2">
                              <h6>Address</h6>
                           </li>
                           <li>
                              <p>58 ถนนหัสดีเสวี ต.ช้างเผือก อ.เมือง จ.เชียงใหม่ 50300</p>
                           </li>
                        </ul>
                        <ul>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="footer-info-bottom col-lg-4 col-md-4">
                  <h4 class="pb-lg-4 pb-md-3 pb-3">Phone</h4>
                  <div class="footer-office-hour">
                     <ul>
                        <li>
                           <p>Phone</p>
                        </li>
                        <li class="my-1">
                           <p>085 614 0001</p>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="footer-info-bottom col-lg-4 col-md-5">
                  <h4 class="pb-lg-4 pb-md-3 pb-3">Contact</h4>
                  <div class="icons mt-3  ">
                     <ul>
                        <li><a target="_blank" href="https://www.facebook.com/Mobile-corner-%E0%B8%A8%E0%B8%B9%E0%B8%99%E0%B8%A2%E0%B9%8C%E0%B8%8B%E0%B9%88%E0%B8%AD%E0%B8%A1%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%96%E0%B8%B7%E0%B8%AD%E0%B9%80%E0%B8%8A%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B9%83%E0%B8%AB%E0%B8%A1%E0%B9%88-%E0%B8%AA%E0%B8%B2%E0%B8%82%E0%B8%B2%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B9%81%E0%B8%A2%E0%B8%81%E0%B8%95%E0%B8%B8%E0%B9%8A%E0%B8%81%E0%B8%95%E0%B8%B2-517525335035367"><span class="fa fa-facebook"> </span></a></li>
                        <div class="footer-office-hour"><li class="my-1">
                           <p>Mobile-corner-ศูนย์ซ่อมมือถือเชียงใหม่-สาขาสี่แยกตุ๊กตา</p>
                           </li>
                     </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!-- move icon -->
            <div class="txt-center">
               <a href="#home" class="move-top txt-center mt-3"></a>
            </div>
            <!--//move icon -->
         </div>
      </footer>
      <!--//footer -->
	  </html>