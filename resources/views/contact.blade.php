@extends('master')
@section('content')
<section class="contact py-lg-4 py-md-3 py-sm-3 py-3" id="contact">
         <div class="container py-lg-5 py-md-4 py-sm-4 py-3">
            <h3 class="title text-center mb-md-4 mb-sm-3 mb-3 mb-2">contact Us</h3>

            <div class="row mt-lg-5 mt-md-4 mt-3">
               <div class="col-lg-6 col-md-6 col-sm-6 address_mail_footer_grids">
                  <iframe src="https://maps.google.com/maps?q=%2058%20%E0%B8%96%E0%B8%99%E0%B8%99%E0%B8%AB%E0%B8%B1%E0%B8%AA%E0%B8%94%E0%B8%B5%E0%B9%80%E0%B8%AA%E0%B8%A7%E0%B8%B5%20%E0%B8%95%E0%B8%B3%E0%B8%9A%E0%B8%A5%E0%B8%8A%E0%B9%89%E0%B8%B2%E0%B8%87%E0%B9%80%E0%B8%9C%E0%B8%B7%E0%B8%AD%E0%B8%81%20%E0%B8%AD%E0%B8%B3%E0%B9%80%E0%B8%A0%E0%B8%AD%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%8A%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B9%83%E0%B8%AB%E0%B8%A1%E0%B9%88%20%E0%B9%80%E0%B8%8A%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B9%83%E0%B8%AB%E0%B8%A1%E0%B9%88%2050300&t=&z=11&ie=UTF8&iwloc=&output=embed"></iframe>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 address-grid">
                  <div class="row address-contact-form">
                     <div class="col-lg-3 col-md-4 col-sm-4">
                        <div class="footer-icon text-center">
                           <span class="fa fa-home" aria-hidden="true"></span>
                        </div>
                     </div>
                     <div class=" footer-contact-list text-center col-lg-9 col-md-8 col-sm-8">
                        <p>58 ถนนหัสดีเสวี ต.ช้างเผือก อ.เมือง จ.เชียงใหม่ 50300</p>
                     </div>
                  </div>
                  <div class="row address-contact-form mt-lg-4 mt-3">
                     <div class="col-lg-3 col-md-4 col-sm-4">
                        <div class="footer-icon text-center">
                           <span class="fa fa-phone" aria-hidden="true"></span>
                        </div>
                     </div>
                     <div class=" footer-contact-list text-center col-lg-9 col-md-8 col-sm-8">
                        <p>085 614 0001</p>
                     </div>
                  </div>
                  <div class="row address-contact-form mt-lg-4 mt-3">
                     <div class="col-lg-3 col-md-4 col-sm-4">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
@stop